# Laporan Resmi

Ini adalah laporan resmi Modul 3 untuk mata kuliah Sistem Operasi tahun 2022.

**Kelas**: B<br>
**Kelompok**: B03<br>
**Modul**: 3<br>

## Anggota

- Muhamad Ridho Pratama (5025201186)
- Naily Khairiya (5025201244)
- Beryl (5025201029)

## Soal

Soal dapat dilihat di
[sini](https://docs.google.com/document/d/1w3tYGgqIkHRFoqKJ9nKzwGLCbt30HM7S)
(harus dengan email ITS).

## Dokumentasi

Berikut adalah dokumentasi, cara pengerjaan, dan kendala selama pengerjaan.

### Screenshot

Jika gambar tidak muncul, buka *link* Google Drive pada [README](./README.md)

#### **Soal 1**

#### **Soal 2**

#### **Soal 3**

### Cara Pengerjaan

#### **Soal 1**
1. Membuat fungsi `main` yang digunakan untuk memanggil fungsi, membuat `thread`, dan men-join-kan `thread`
```c
int main(void)
{
    int i = 0;
    int err;
    while (i < 2) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &unzip, NULL); // membuat thread
        if (err != 0)                                        // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // buat folder hasil
    createFolder(f_hasil);
    .
    .
    .
}
```

- pada potongan fungsi  `main` diatas terdapat pembuatan `thread` dengan menggunakan `while i<2` yang akan diiterasikan sebanyak 2 kali
- di luar `while` terdapat   `pthread_join(tid[0], NULL);` yang digunakan untuk menjoinkan setiap `thread`
- Pada soal ini menggunakan thread antara lain untuk mendownload dan mengekstrak 2 file, men-decode file pada 2 folder, memasukkan setiap file txt ke folder hasil, serta untuk mengekstrak untuk kedua kalinya secara bersamaan.
- pada potongan program diatas juga ditunjukkan contoh pemanggilan fungsi. Yaitu `createFolder(f_hasil);`
- contoh lain digunakan untuk memanggil fungsi `zip()`, `zip2();`


2. Fungsi `unzip()`
```c
void *
unzip(void *arg)
{
    int n = 3, status;
    pid_t child_id[4];
    unsigned long i = 0;
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {

        for (int i = 0; i < n; i++)
        {

            child_id[i] = fork();

            if (child_id[i] < 0) // gagal
            {
                exit(EXIT_FAILURE);
            }
            else if (i == 0 && child_id[i] == 0) // download
            {
                char *argv[] = {"wget", "-q", url[0], "-O", fileName[0], NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child_id[i] == 0) // buat folder
            {
                char *argv[] = {"mkdir", "-p", folder[0], NULL};
                execv("/bin/mkdir", argv);
            }
            else if (i == 2 && child_id[i] == 0)
            {
                sleep(10);
                char *argv[] = {"unzip", "-q", fileName[0], "-d", folder[0], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {
        .
        .
        .
    }
    return NULL;
}

```
- pada fungsi  `unzip()` terdapat 2 thread. Yang sama-sama melakukan hal yang sama namun pada file yang berbeda. file pertama untuk music satunya untuk quote. 
- pada setiap  `thread` melakukan download dengan `execv wget`, membuat folder untuk menyimpan hasil ekstrak, serta untuk mengekstrak sendiri dengan `execv unzip`

3. Fungsi `base64Decoder()` untuk melakukan decode
- apabila menggunakan `base64 --decode /path/ke/file` belum dapat menyimpan output ke text file
- jadi fungsi ini dibuat secara manual

4. fungsi `listFilesRecursively()`
```c
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(list_0[count], basePath);
            strcat(list_0[count], "/");
            strcat(list_0[count], dp->d_name);
            count++;

            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            listFilesRecursively(path);
        }
    }

    closedir(dir);
}

```
- untuk list file yang ada pada suatu folder
- pada setiap nama file akan ditambahkan path dari base path dengan menggunakan `strcat()`
- setiap path file akan disimpan pada array `list_0[]`


5. Membuat fungsi `createFolder`
- fungsi ini digunakan untuk membuat folder
- pada kasus ini digunakan untuk membuat folder hasil
- dengan menggunakan `execv` dan `mkdir`

6. Membuat fungsi `hasil()`
```c
void *
hasil(void *arg)
{
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {
        pid_t child_id;
        int status;

        child_id = fork();

        // jika fork tidak berhasil
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
            char *argv[] = {"find", fileTxt[0], "-type", "f", "-exec", "mv", "{}", f_hasil, ";", NULL};
            execv("/usr/bin/find", argv);
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {
        .
        .
        .
    }
    return NULL;
}
```

- fungsi `hasil()` digunakan untuk memindahkan file `music.txt` dan `quote.txt` ke folder hasil secara bersamaan
- untuk melakukannya secara bersamaan digunakan `thread`
- `thread` pertama digunakan untuk memindahkan file `music.txt` dan `thread` kedua digunakan untuk memindahkan file `quote.txt`
- pemindahan dilakukan dengan menggunakan `exec` dan `find`

7. Pembuatan fungsi `zip()`
- fungsi zip digunakan untuk men-zip folder hasil dengan password
- digunakan `execv` dan `zip`
- menggunakan `-e` untuk enkripsi atau memberikan password

8. Membuat fungsi `unzipKedua()`
```c
void *
unzipKedua(void *arg)
{
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {
        pid_t child_id;
        int status;

        child_id = fork();

        // jika fork tidak berhasil
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
            char *argv[] = {"unzip", "-q", "/home/naily/hasil.zip", NULL};
            execv("/usr/bin/unzip", argv);
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {
        pid_t child_id[3];
        int status;

        for(int j=0; j<2; j++)
        {
            child_id[j] = fork();

            // jika fork tidak berhasil
            if (child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            else
            if (child_id[j] == 0 && j==0)
            {
                char *argv[] = {"touch", "/home/naily/no.txt", NULL};
                execv("/usr/bin/touch", argv);
            }
            else
            if(j==1 && child_id[j] == 0)
            {
                sleep(2);
                FILE *fp = fopen("/home/naily/no.txt", "w");

                if (fp == NULL)
                {
                    printf("ERROR SAAT MEMBUKA FILE\n");
                    exit(EXIT_FAILURE);
                }

                fprintf(fp, "No\n");
                fclose(fp);
            }
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    return NULL;
}
```
- pada fungsi `unzipKedua()` ini, menggunakan dua `thread`.  `thread` pertama digunakan untuk unzip file `hasil.zip`
- untuk unzip menggunakan `execv` dan `unzip`
- sedangkan untuk `thread` kedua, digunakan untuk membuat file `no.txt` dengan isi `No`

9. Membuat fungsi `zip2()`
- fungsi ini digunakan untuk zip folder hasil dan juga file    `no.txt` 
- menggunakan `execv` dan juga `zip`


#### **Soal 2**

#### **Soal 3**
1. Melakukan unzip yang diberikan ke dalam folder “/home/[user]/shift3/”
```c
void extract()
{
    	pid_t child_id;
    	int status;
    	child_id = fork();

    	if (child_id < 0)
    	{
    	    exit(EXIT_FAILURE);
    	}	

    	if (child_id == 0)
    	{
        	char *argv[] = {"unzip", "-q", "/home/archblaze/hartakarun.zip", "-d", "/home/archblaze/shift3", NULL};
        	execv("/usr/bin/unzip", argv);
    	}
    	while (wait(&status) > 0);
}
```
2.Kategorisasi folder dan file
- Mengecek eksistensi file
```c
int fileexistency(const char *filename)
{
    	struct status buffer;
    	int exists = status(filename, &buffer);
    	if (exists == 0)
    	{
        	return 1;
        }

    	else return 0;
}
```
- Mengkategorisasikan file berdasarkan format file
```c
void movefile(void filename)
{
		int i;
		char cwd[PATH_MAX];
    	char dirname[100];
   		char hidden[100];
    	char file[100];
    	char hiddenfilename[100];
    	char fileexists[100];

    	strcpy(fileexists, filename);
    	strcpy(hiddenfilename, filename);

    	char *name = strchr(hiddenfilename, '/');
    	strcpy(hidden, name);
    	
    	//normal
    	if (strstr(filename, ".") != NULL)
    	{
    		strcpy(file, filename);
        	strtok(file, ".");
        	char *tok = strtok(NULL, "");

        	for(i = 0; tok[i]; i++)
        	{
            		tok[i] = tolower(tok[i]);
        	}
            strcpy(dirname, tok);
    	}

    	//hidden
    	else if(hidden[1] == '.')
    	{
        	strcpy(dirname, "Hidden");
    	}

    	//unknown
    	else
    	{
        	strcpy(dirname, "Unknown");
    	}

    	if(getcwd(cwd, sizeof(cwd)) != NULL)
    	{
        	char *foldername = strchr(filename, '/');
        	char namefile[100];
        	strcpy(namefile, cwd);
			strcat(namefile, "/");
			strcat(namefile, dirname);
			strcat(namefile, foldername);
			rename(filename, namefile);
		}
}
```

3. Main function
```c
int main(int argc, char* argv[])
{
	extract();
	fileexistency();
	movefile();
	categorize();
	zip();
}
```

4. Melakukan zip terhadap folder yang berisi hasil kategorisasi
```c
void zip()
{
	    pid_t child_id;
    	int status;
    	child_id = fork();

    	if (child_id < 0)
    	{
    	    exit(EXIT_FAILURE);
    	}	

    	if (child_id == 0)
    	{
        	char *argv[] = {"zip", "-q", "-r", "-m", "/home/archblaze/shift3/hartakarun.zip", "/home/archblaze/shift3", NULL};
        	execv("/usr/bin/zip", argv);
    	}
    	while (wait(&status) > 0);
}
```

5. Server dan Client untuk mengirimkan dan menerima file yang sudah di zip 
- Server
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#define SIZE 1024

void write_file(int sockfd){
  int n;
  FILE *fp;
  char *filename = "hartakaruntest.zip";
  char buffer[SIZE];

  fp = fopen(filename, "w");
  while (1) {
    n = recv(sockfd, buffer, SIZE, 0);
    if (n <= 0){
      break;
      return;
    }
    fprintf(fp, "%s", buffer);
    bzero(buffer, SIZE);
  }
  return;
}

int main(){
  char *ip = "127.0.0.1";
  int port = 8080;
  int e;

  int sockfd, new_sock;
  struct sockaddr_in server_addr, new_addr;
  socklen_t addr_size;
  char buffer[SIZE];

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+]Server socket created successfully.\n");

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port;
  server_addr.sin_addr.s_addr = inet_addr(ip);

  e = bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
  if(e < 0) {
    perror("[-]Error in bind");
    exit(1);
  }
  printf("[+]Binding successfull.\n");

  if(listen(sockfd, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		perror("[-]Error in listening");
    exit(1);
	}

  addr_size = sizeof(new_addr);
  new_sock = accept(sockfd, (struct sockaddr*)&new_addr, &addr_size);
  write_file(new_sock);
  printf("[+]Data written in the file successfully.\n");

  return 0;
}
```

- Client
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#define SIZE 1024

void send_file(FILE *fp, int sockfd){
  int n;
  char data[SIZE] = {0};

  while(fgets(data, SIZE, fp) != NULL) {
    if (send(sockfd, data, sizeof(data), 0) == -1) {
      perror("[-]Error in sending file.");
      exit(1);
    }
    bzero(data, SIZE);
  }
}

int main(){
  char *ip = "127.0.0.1";
  int port = 8080;
  int e;

  int sockfd;
  struct sockaddr_in server_addr;
  FILE *fp;
  char *filename = "hartakarun.zip";

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0) {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+]Server socket created successfully.\n");

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = port;
  server_addr.sin_addr.s_addr = inet_addr(ip);

  e = connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
  if(e == -1) {
    perror("[-]Error in socket");
    exit(1);
  }
	printf("[+]Connected to Server.\n");

  fp = fopen(filename, "r");
  if (fp == NULL) {
    perror("[-]Error in reading file.");
    exit(1);
  }

  send_file(fp, sockfd);
  printf("[+]File data sent successfully.\n");

	printf("[+]Closing the connection.\n");
  close(sockfd);

  return 0;
}
```

### Kendala Saat Pengerjaan

#### **Soal 1**
- Belum terlalu tahu penggunaan base64 yang dari linux tidak dapat menyimpan output ke text file, ada error base64 : extra operand
- Ada kendala pada pembuatan text file yang tidak bisa mengeprint "No" diawal

#### **Soal 2**

#### **Soal 3**
- Belum paham cara untuk melakukan unzip tanpa menggunakan fork

## Referensi
- [Google](https://www.google.com)
- [StackOverflow](https://stackoverflow.com)
- [Unix & Linux Stack Exchange](https://unix.stackexchange.com/)
- [GitHub Modul 3 Sistem Operasi](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul3)
