#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <malloc.h>

pthread_t tid[3];
char *url[] = {"https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download",
               "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"};

char *fileName[] = {"/home/naily/music.zip", "/home/naily/quote.zip"};
char *folder[] = {"/home/naily/music", "/home/naily/quote"};
char *fileTxt[] = {"/home/naily/music.txt", "/home/naily/quote.txt"};
char list_0[20][100] = {};
char *f_hasil = {"/home/naily/hasil"};
int count = 0;

void *
unzip(void *arg)
{
    int n = 3, status;
    pid_t child_id[4];
    unsigned long i = 0;
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {

        for (int i = 0; i < n; i++)
        {

            child_id[i] = fork();

            if (child_id[i] < 0) // gagal
            {
                exit(EXIT_FAILURE);
            }
            else if (i == 0 && child_id[i] == 0) // download
            {
                char *argv[] = {"wget", "-q", url[0], "-O", fileName[0], NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child_id[i] == 0) // buat folder
            {
                char *argv[] = {"mkdir", "-p", folder[0], NULL};
                execv("/bin/mkdir", argv);
            }
            else if (i == 2 && child_id[i] == 0)
            {
                sleep(10);
                char *argv[] = {"unzip", "-q", fileName[0], "-d", folder[0], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {

        for (int i = 0; i < n; i++)
        {
            child_id[i] = fork();

            if (child_id[i] < 0) // gagal
            {
                exit(EXIT_FAILURE);
            }
            else if (i == 0 && child_id[i] == 0) // download
            {

                char *argv[] = {"wget", "-q", url[1], "-O", fileName[1], NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child_id[i] == 0) // buat folder
            {

                char *argv[] = {"mkdir", "-p", folder[1], NULL};
                execv("/bin/mkdir", argv);
            }
            else if (i == 2 && child_id[i] == 0)
            {

                sleep(10);
                char *argv[] = {"unzip", "-q", fileName[1], "-d", folder[1], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
        while (wait(&status) > 0)
            ;
    }
    return NULL;
}

char *base64Decoder(char encoded_str[], int len)
{
    char *decoded_str;

    decoded_str = (char *)malloc(sizeof(char) * 300);

    int i, j, k = 0;

    int num = 0;

    int cnt_bits = 0;

    for (i = 0; i < len; i += 4)
    {
        num = 0, cnt_bits = 0;
        for (j = 0; j < 4; j++)
        {
            if (encoded_str[i + j] != '=')
            {
                num = num << 6;
                cnt_bits += 6;
            }

            if (encoded_str[i + j] >= 'A' && encoded_str[i + j] <= 'Z')
                num = num | (encoded_str[i + j] - 'A');

            else if (encoded_str[i + j] >= 'a' && encoded_str[i + j] <= 'z')
                num = num | (encoded_str[i + j] - 'a' + 26);

            else if (encoded_str[i + j] >= '0' && encoded_str[i + j] <= '9')
                num = num | (encoded_str[i + j] - '0' + 52);

            else if (encoded_str[i + j] == '+')
                num = num | 62;

            else if (encoded_str[i + j] == '/')
                num = num | 63;

            else
            {
                num = num >> 2;
                cnt_bits -= 2;
            }
        }

        while (cnt_bits != 0)
        {
            cnt_bits -= 8;

            decoded_str[k++] = (num >> cnt_bits) & 255;
        }
    }

    decoded_str[k] = '\0';

    return decoded_str;
}

void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(list_0[count], basePath);
            strcat(list_0[count], "/");
            strcat(list_0[count], dp->d_name);
            count++;

            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            listFilesRecursively(path);
        }
    }

    closedir(dir);
}

void *decodeTxt(void *arg)
{
    int n = 3, status;
    pid_t child_id[4];
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {

        for (int i = 0; i < n; i++)
        {

            child_id[i] = fork();

            if (child_id[i] < 0) // gagal
            {
                exit(EXIT_FAILURE);
            }
            else if (i == 0 && child_id[i] == 0) // buat file txt music
            {

                char *argv[] = {"touch", fileTxt[0], NULL};
                execv("/usr/bin/touch", argv);
            }
            else if (i == 1 && child_id[i] == 0) // buat list file & decode
            {

                count = 0;
                listFilesRecursively(folder[0]);
                for (int j = 0; j < count; j++)
                {
                    char *filename = list_0[j];
                    FILE *fp = fopen(filename, "r+");

                    if (fp == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }

                    char buff[256];

                    fgets(buff, 255, fp);

                    fclose(fp);

                    FILE *fp2 = fopen(filename, "w+");
                    FILE *fp3 = fopen(fileTxt[0], "a");

                    if (fp2 == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }
                    if (fp3 == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }

                    fprintf(fp2, "%s\n", base64Decoder(buff, strlen(buff)));
                    fprintf(fp3, "%s\n", base64Decoder(buff, strlen(buff)));
                    fclose(fp2);
                    fclose(fp3);
                }
            }
        }
        while (wait(&status) > 0)
            ;
    }

    else if (pthread_equal(id, tid[1])) // file 2
    {
        for (int i = 0; i < n; i++)
        {

            child_id[i] = fork();

            if (child_id[i] < 0) // gagal
            {
                exit(EXIT_FAILURE);
            }
            else if (i == 0 && child_id[i] == 0) // buat file txt music
            {

                char *argv[] = {"touch", fileTxt[1], NULL};
                execv("/usr/bin/touch", argv);
            }
            else if (i == 1 && child_id[i] == 0) // buat list file & decode
            {

                count = 0;
                listFilesRecursively(folder[1]);
                for (int j = 0; j < count; j++)
                {
                    char *filename = list_0[j];
                    FILE *fp = fopen(filename, "r+");

                    if (fp == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }

                    char buff[256];

                    fgets(buff, 255, fp);

                    fclose(fp);

                    FILE *fp2 = fopen(filename, "w+");
                    FILE *fp3 = fopen(fileTxt[1], "a");

                    if (fp2 == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }
                    if (fp3 == NULL)
                    {
                        printf("ERROR SAAT MEMBUKA FILE\n");
                        exit(EXIT_FAILURE);
                    }

                    fprintf(fp2, "%s\n", base64Decoder(buff, strlen(buff)));
                    fprintf(fp3, "%s\n", base64Decoder(buff, strlen(buff)));
                    fclose(fp2);
                    fclose(fp3);
                }
            }
        }
        while (wait(&status) > 0)
            ;
    }

    return NULL;
}

void createFolder(char dir[])
{
    pid_t child_id = fork();
    int status;

    if (child_id < 0)
    {
        // Jika gagal membuat proses baru, program akan berhenti
        exit(EXIT_FAILURE);
    }

 
    if (child_id == 0)
    {
        char *argv[] = {"mkdir", "-p", dir, NULL};
        execv("/bin/mkdir", argv);
    }

    // pakai wait supaya bisa jalanin fungsi selanjutnya kalau sudah selesai
    while (wait(&status) > 0)
        ;
}

void *
hasil(void *arg)
{
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {
        pid_t child_id;
        int status;

        child_id = fork();

        // jika fork tidak berhasil
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
            char *argv[] = {"find", fileTxt[0], "-type", "f", "-exec", "mv", "{}", f_hasil, ";", NULL};
            execv("/usr/bin/find", argv);
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {
        pid_t child_id;
        int status;

        child_id = fork();

        // jika fork tidak berhasil
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
            char *argv[] = {"find", fileTxt[1], "-type", "f", "-exec", "mv", "{}", f_hasil, ";", NULL};
            execv("/usr/bin/find", argv);
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    return NULL;
}

void zip()
{
    pid_t child_id;
    int status;

    child_id = fork();

    // jika fork tidak berhasil
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
        char *argv[] = {"zip", "-e", "-q", "-r", "-m", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip", argv);
    }

    // wait supaya bisa lanjut ke proses selanjutnya
    while (wait(&status) > 0)
        ;
}

void *
unzipKedua(void *arg)
{
    pthread_t id = pthread_self();

    if (pthread_equal(id, tid[0])) // file1
    {
        pid_t child_id;
        int status;

        child_id = fork();

        // jika fork tidak berhasil
        if (child_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (child_id == 0)
        {
            char *argv[] = {"unzip", "-q", "/home/naily/hasil.zip", NULL};
            execv("/usr/bin/unzip", argv);
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    else if (pthread_equal(id, tid[1])) // file 2
    {
        pid_t child_id[3];
        int status;

        for(int j=0; j<2; j++)
        {
            child_id[j] = fork();

            // jika fork tidak berhasil
            if (child_id < 0)
            {
                exit(EXIT_FAILURE);
            }
            else
            if (child_id[j] == 0 && j==0)
            {
                char *argv[] = {"touch", "/home/naily/no.txt", NULL};
                execv("/usr/bin/touch", argv);
            }
            else
            if(j==1 && child_id[j] == 0)
            {
                sleep(2);
                FILE *fp = fopen("/home/naily/no.txt", "w");

                if (fp == NULL)
                {
                    printf("ERROR SAAT MEMBUKA FILE\n");
                    exit(EXIT_FAILURE);
                }

                fprintf(fp, "No\n");
                fclose(fp);
            }
        }

        // wait supaya bisa lanjut ke proses selanjutnya
        while (wait(&status) > 0)
            ;
    }
    return NULL;
}

void zip2()
{
    pid_t child_id;
    int status;

    child_id = fork();

    // jika fork tidak berhasil
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
        char *argv[] = {"zip", "-e", "-q", "-r", "-m", "hasil.zip", "hasil", "no.txt", NULL};
        execv("/usr/bin/zip", argv);
    }

    // wait supaya bisa lanjut ke proses selanjutnya
    while (wait(&status) > 0)
        ;
}

int main(void)
{
    int i = 0;
    int err;
    while (i < 2) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &unzip, NULL); // membuat thread
        if (err != 0)                                        // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // loop 2 untuk decode
    i = 0;
    while (i < 2) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &decodeTxt, NULL); // membuat thread
        if (err != 0)                                            // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // buat folder hasil
    createFolder(f_hasil);
    // pindahkan ke folder hasil
    // loop 2 untuk masukkan folder ke f_hasil
    i = 0;
    while (i < 2) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &hasil, NULL); // membuat thread
        if (err != 0)                                        // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // hasil di zip dengan password  disini misal pakai mihinomenestnaily
    zip();

    i = 0;
    while (i < 2) // loop sejumlah thread
    {
        err = pthread_create(&(tid[i]), NULL, &unzipKedua, NULL); // membuat thread
        if (err != 0)                                             // cek error
        {
            printf("\n can't create thread : [%s]", strerror(err));
        }
        else
        {
            printf("\n create thread success\n");
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    zip2();
    return 0;
}
