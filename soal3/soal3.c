#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>

void extract()
{
    	pid_t child_id;
    	int status;
    	child_id = fork();

    	if (child_id < 0)
    	{
    	    exit(EXIT_FAILURE);
    	}	

    	if (child_id == 0)
    	{
        	char *argv[] = {"unzip", "-q", "/home/archblaze/hartakarun.zip", "-d", "/home/archblaze/shift3", NULL};
        	execv("/usr/bin/unzip", argv);
    	}
    	while (wait(&status) > 0);
}



void zip()
{
	    pid_t child_id;
    	int status;
    	child_id = fork();

    	if (child_id < 0)
    	{
    	    exit(EXIT_FAILURE);
    	}	

    	if (child_id == 0)
    	{
        	char *argv[] = {"zip", "-q", "-r", "-m", "/home/archblaze/shift3/hartakarun.zip", "/home/archblaze/shift3", NULL};
        	execv("/usr/bin/zip", argv);
    	}
    	while (wait(&status) > 0);
}



/*void categorize()
{

}*/

int main(int argc, char* argv[])
{
	extract();
	zip();
}